class Dove:

    def fly(self):
        print('Dove can fly')

    def swim(self):
        print('Dove cannot swim')   

class Penguin():
   
    def fly(self):
        print('Penguin cannot fly')

    def swim(self):
        print ('Penguin can swim')  

def flyingtest(bird):
    bird.swim()

green = Dove() 
red = Penguin()

flyingtest(green)
flyingtest(red)