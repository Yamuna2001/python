import turtle
import time
import sys
import gc

sys.setrecursionlimit(1500)

screen = turtle.Screen()

mario = turtle.Turtle()
sky = turtle.Turtle()
sky2 = turtle.Turtle()
start_screen = turtle.Turtle()
pipe = turtle.Turtle()
lives_text = turtle.Turtle()
coins_text = turtle.Turtle()
ground = turtle.Turtle()
goomba = turtle.Turtle()
block = turtle.Turtle()
level2 = turtle.Turtle()
level3 = turtle.Turtle()
bowser = turtle.Turtle()


bowser.hideturtle()
level2.hideturtle()
level3.hideturtle()
block.hideturtle()
goomba.hideturtle()

screen.addshape("block.gif")
screen.addshape("bowser.gif")
screen.addshape("coin.gif")
screen.addshape("end_frame_1.gif")
screen.addshape("end_frame_2.gif")
screen.addshape("end_frame_3.gif")
screen.addshape("end_frame_4.gif")
screen.addshape("end_frame_5.gif")
screen.addshape("end_frame_6.gif")
screen.addshape("end_frame_7.gif")
screen.addshape("end_frame_8.gif")
screen.addshape("goomba_frame_1.gif")
screen.addshape("goomba_frame_2.gif")
screen.addshape("ground.gif")
screen.addshape("idle.gif")
screen.addshape("level_2.gif")
screen.addshape("level_3.gif")
screen.addshape("mario_head.gif")
screen.addshape("mario_jump_left.gif")
screen.addshape("mario_jump.gif")
screen.addshape("mario_run_left.gif")
screen.addshape("mario_run.gif")
screen.addshape("pipe_side.gif")
screen.addshape("pipe.gif")
screen.addshape("sky.gif")
screen.addshape("StartScreen.gif")
screen.addshape("upside_down_pipe.gif")
screen.addshape("game_over.gif")

move_speed = 35
positionX = -400
positionY = -130
coin_amount = 0
sky_movement = 0
level = 1
lives = 3

screen.setup(960,540)

def load_start_screen():
    start_screen.shape("StartScreen.gif")
    turtle.onscreenclick(load_game)




is_jumping = False
is_hit_by_enemy = False


coins = [None,None,None,None,None]
for i in range(5):
    if i < len(coins):
        coins[i] =  turtle.Turtle()




def run():
    global coin_amount
    global coins
    global is_hit_by_enemy
    if is_collided_with(mario, goomba) and goomba.isvisible() and level is 2 :
        if is_hit_by_enemy :
            return
        is_hit_by_enemy = True
        lose_life()
    for i in range(len(coins)):
        coins[i].hideturtle()
        del coins[i]
        gc.collect()
        style = ('Courier',30,'bold')
        coin_amount += 1
        coins_text.clear()
        coins_text.write("x" + str(coin_amount),font=style, align='center')
        time.sleep(0.2)
        coins_text.clear()
        style = ('Courier',20,'bold')
        coins_text.write("x" + str(coin_amount),font=style, align='center')
        

def right():
    global mario
    global positionX
    global pipe
    global level
    
    if is_jumping:
        return

    if level is 1 and is_collided_with(mario, pipe):
        return
        
    if level is 2 and is_collided_with(mario, pipe):
        level = 3
        level3.showturtle()
        level3.speed(3)
        level3.goto(0,0)
        bowser.up()
        bowser.shape("bowser.gif")
        bowser.showturtle()
        bowser.goto(330, -100)
        redraw_gui()
        mario = turtle.Turtle()
        mario.shape("idle.gif")
        mario.up()
        mario.goto(-400, -130)
        block.shape("block.gif")
        block.up()
        block.showturtle()
        block.goto(-350, 50)
        move_bowser()
        turtle.mainloop()

        

    mario.forward(move_speed)
    positionX = mario.xcor()
    mario.shape("mario_run.gif")
    run()

def left():
    global mario
    global position

    if is_jumping :
        return

    mario.backward(move_speed)
    positionX = mario.xcor()
    mario.shape("mario_run_left.gif")
    run()

def jump():
    global positionX
    global positionY
    global is_jumping
    global mario

    if is_jumping :
        return
    is_jumping = True
    positionY = mario.ycor()
    turtle.delay(10)
    mario.left(90)
    mario.shape("mario_jump.gif")
    mario.forward(150)
    if level is 3 and is_collided_with(mario, block):
        bg = turtle.Turtle()
        bg.shape("end_frame_1.gif")
        time.sleep(0.5)
        bg.shape("end_frame_2.gif")
        time.sleep(0.5)
        bg.shape("end_frame_3.gif")
        time.sleep(0.5)
        bg.shape("end_frame_4.gif")
        time.sleep(0.5)
        bg.shape("end_frame_5.gif")
        time.sleep(0.5)
        bg.shape("end_frame_6.gif")
        time.sleep(0.5)
        bg.shape("end_frame_7.gif")
        time.sleep(0.5)
        bg.shape("end_frame_8.gif")
        time.sleep(0.5)

    mario.right(90)
    mario.forward(120)
    mario.left(90)
    positionX = mario.xcor()
    turtle.ontimer(idle, 500)

def idle():
    global is_jumping
    global mario
    mario.shape("idle.gif")
    mario.goto(positionX, positionY)
    mario.seth(0)
    is_jumping = False
    turtle.delay(0)

def load_game(x,y):
    global start_screen
    global ground
    turtle.onscreenclick(None)
    start_screen.hideturtle()
    show_sky()
    ground.shape("ground.gif")
    mario.shape("idle.gif")
    mario.up()
    mario.goto(-400,-130)
    redraw_gui()
    pipe.shape("pipe.gif")
    pipe.up()
    pipe.goto(380,-120)
    layout_coins()
    scroll_bg()
    
def redraw_gui():
    global coin_amount
    mario_head = turtle.Turtle()
    mario_head.shape("mario_head.gif")
    mario_head.up()
    mario_head.goto(-430,230)
    coin_gui = turtle.Turtle()
    coin_gui.shape("coin.gif")
    coin_gui.up()
    coin_gui.goto(-300,230)
    lives_text.up()
    lives_text.hideturtle()
    lives_text.goto(-370,210)
    lives_text.color('black')
    style = ('Courier',20,'bold')
    lives_text.write('x 3', font=style, align='center')
    coins_text.up()
    coins_text.hideturtle()
    coins_text.goto(-220,220)
    coins_text.color('black')
    coins_text.write('x ' + str(coin_amount), font=style, align='center')

def layout_coins():
    x = -200
    for i in range(len(coins)):
        coins[i].up()
        coins[i].shape("coin.gif")
        coins[i].goto(x, -160)
        x += 100

def show_sky():
    global sky
    global sky2
    sky.shape("sky.gif")
    sky.up()
    sky2.up()
    sky2.shape("sky.gif")
    sky2.hideturtle()
    sky2.goto(540,0)
    sky2.showturtle()

def scroll_bg():
    global sky
    global sky2
    global sky_movement
    if not show_sky:
        return
    sky_movement += 1
    sky.speed(1)
    sky2.speed(1)
    sky.backward(1)
    sky2.backward(1)
    if sky.xcor() < -500 :
        sky.speed(100)
        sky2.speed(100)
        sky.goto(0,0)
        sky2.goto(540,0)
    if mario.distance(pipe) < 30 :
        load_level_2()
    turtle.ontimer(scroll_bg(), 50)

def is_collided_with(a,b):
    return abs(a.xcor() - b.xcor()) < 150

def load_level_2() :
    global mario
    global sky
    global sky2
    global pipe
    global goomba
    global ground
    global level
    global level2
    global level3
    turtle.delay(0)
    ground.hideturtle()
    sky.hideturtle()
    sky2.hideturtle()
    pipe.hideturtle()
    mario.hideturtle()
    del ground
    del sky
    del sky2
    del mario
    level = 2
    level2.showturtle()
    level2.up()
    level2.shape("level_2.gif")
    mario = turtle.Turtle()
    mario.up()
    mario.hideturtle()
    mario.goto(-430, 230)
    mario.shape("idle.gif")
    mario.speed(2)
    upside_down_pipe = turtle.Turtle()
    upside_down_pipe.up()
    upside_down_pipe.shape("upside_down_pipe.gif")
    upside_down_pipe.goto(-430, 230)
    redraw_gui()
    time.sleep(0.5)
    mario.showturtle()
    turtle.delay(10)
    mario.goto(-400, -170)
    turtle.delay(0)

    goomba.up()
    goomba.shape("goomba_frame_1.gif")
    goomba.hideturtle()
    goomba.goto(400, -200)
    goomba.showturtle()

    pipe_side = turtle.Turtle()
    pipe_side.speed(40)
    pipe_side.up()
    pipe_side.shape("pipe_side.gif")
    pipe_side.hideturtle()
    pipe_side.goto(400, -200)
    pipe_side.showturtle()

    level3.up()
    level3.hideturtle()
    level3.shape("level_3.gif")
    level3.goto(950, 0)

    goomba.speed(40)
    move_goomba()
    turtle.mainloop()


def move_goomba():
    global goomba
    goomba.shape("goomba_frame_1.gif")
    time.sleep(0.25)
    goomba.shape("goomba_frame_2.gif")
    time.sleep(0.25)
    goomba.backward(50)
    turtle.ontimer(move_goomba(), 50)

def lose_life():
    global lives_text
    global lives
    global is_hit_by_enemy
    lives -= 1
    if lives is 0 :
        game_over = turtle.Turtle()
        game_over.shape("game_over.gif")
    style = ('Courier',20,'bold')
    lives_text.clear()
    lives_text.write('x ' + str(lives), font=style, align='center')
    mario.shape("idle.gif")
    mario.goto(-400, -170)
    goomba.goto(400,-200)
    is_hit_by_enemy = False

def move_bowser():
    turtle.delay(5)
    bowser.speed(1)
    bowser.backward(1)
    if is_collided_with(bowser, mario):
        game_over = turtle.Turtle()
        game_over.shape("game_over.gif")
        return
    turtle.ontimer(move_bowser(), 10000)








screen.onkeypress(left, "Left")
screen.onkeypress(right, "Right")
screen.onkeypress(jump, "Up")
screen.onkeyrelease(lambda:mario.shape("idle.gif"), "Left")
screen.onkeyrelease(lambda:mario.shape("idle.gif"), "Right")

screen.listen()
load_start_screen()
screen.mainloop()
