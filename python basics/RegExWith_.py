import re

YamRegEx = re.compile(r'Bat(wo)?man')
msg4 = YamRegEx.search('The adventures of Batman')
print(msg4.group())

msg5 = YamRegEx.search('The adventures of Batwoman')
print(msg5.group())
