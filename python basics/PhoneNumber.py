def isPhoneNumber(text):
     if len(text) != 12:
         return False
     for i in range(0, 3):
          if not text[i].isdecimal():
               return False
     if text[3] != '-':
         return False
     for i in range(4, 7):
          if not text[i].isdecimal():
             return False
     if text[7] != '-':
         return False
     for i in range(8, 12):
          if not text[i].isdecimal():
             return False
     return True

print('123-456-7890 a phone number')
print(isPhoneNumber('123-456-7890'))
print('yamuna a phone number')
print(isPhoneNumber('yamuna'))    


message = 'call me at 123-456-7890 tommorow.  567-321-4589 is my office. 224-789-9807 is my alternative number.'
for i in range(len(message)):
    chunk = message[i:i+12]
    if isPhoneNumber(chunk):
        print('Phone Number Found:' + chunk)
print('We found the phone number')


